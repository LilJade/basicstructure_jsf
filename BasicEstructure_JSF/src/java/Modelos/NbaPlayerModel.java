package Modelos;
import Entidades.NbaPlayer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class NbaPlayerModel {

    List<NbaPlayer> lista = new ArrayList<>();

    public List<NbaPlayer> listaPlayers() {
        if (!lista.isEmpty()) {
            System.out.println(lista.size());
        } else {
            lista.add(new NbaPlayer(1, 24, "Kobe Bryant", "Los Ángeles Lakers"));
            lista.add(new NbaPlayer(2, 44, "Robert Williams", "Boston Celtics"));
            lista.add(new NbaPlayer(3, 23, "Michael Jordan", "Chicago Bulls"));
            lista.add(new NbaPlayer(4, 23, "LeBron James", "Los Angeles Lakers"));
        }
        return lista;
    }

    public void registrar(NbaPlayer p) {
        if(p.getId() > 0 && p.getNombre().length() > 0){
            lista.add(p);
            System.out.println("Agregado");
        }
    }

    public void editar(NbaPlayer p) {
        Iterator<NbaPlayer> i = lista.iterator();
        int index = 0;
        while(i.hasNext()){
            NbaPlayer player = i.next();
            if(player.getId() == p.getId()){
                if(p.getId() > 0 && p.getNombre().length() > 0){
                    lista.set(index, p);
                    System.out.println("Editado");
                }
            }
            index++;
        }
    }

    public void eliminar(NbaPlayer p) {
        Iterator<NbaPlayer> i = lista.iterator();
        while(i.hasNext()){
            NbaPlayer player = i.next();
            
            if(player.getId() == p.getId()){
                System.out.println("ID ELIMINADO: " + player.getId());
                i.remove();
            }
        }
    }

    /**
     * @return NbaPlayer
     */
    public NbaPlayer showMVP() {
        NbaPlayer mvp = new NbaPlayer(1, 24, "Kobe Bryant", "Los Ángeles Lakers");
        return mvp;
    }
}
