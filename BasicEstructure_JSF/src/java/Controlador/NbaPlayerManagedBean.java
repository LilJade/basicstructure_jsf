package Controlador;

import Entidades.NbaPlayer;
import Modelos.NbaPlayerModel;
import java.io.Serializable;
import java.util.List;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;

@Named(value = "BNbaPlayer")
@SessionScoped
public class NbaPlayerManagedBean implements Serializable {

    private NbaPlayer nbap = new NbaPlayer();
    private NbaPlayerModel nbaplayermodel = new NbaPlayerModel();

    public NbaPlayerManagedBean() {
    }

    /**
     * @return the nbap
     */
    public NbaPlayer getNbaPlayer(){
        return nbap;
    }
    
    /**
     * @param nbap the nba to set
     */
    public void setNbaPlayer(NbaPlayer nbap){
        this.nbap = nbap;
    }
    
    /**
     * @return the nbaplayermodel
     */
    public NbaPlayer getShowMVP() {
        return nbaplayermodel.showMVP();
    }

    public String getAgregarPlayer() {
        nbaplayermodel.registrar(this.nbap);
        this.nbap = new NbaPlayer();
        
        return "index?faces-redirect=true";
    }

    public String getEditarPlayer() {
        nbaplayermodel.editar(this.nbap);
        this.nbap = new NbaPlayer();
        
        return "index?faces-redirect=true";
    }

    public String getEliminarPlayer() {
        nbaplayermodel.eliminar(this.nbap);
        this.nbap = new NbaPlayer();
        
        return "index?faces-redirect=true";
    }
    
    public List<NbaPlayer> getListaNbaPlayer() {
        return nbaplayermodel.listaPlayers();
    }
}
