
package Entidades;

public class NbaPlayer {
      
    //propiedades
    private int Id;
    private int Numero;
    private String Nombre;
    private String Equipo;
    
    //constructores

    public NbaPlayer() {
    }

    public NbaPlayer(String Nombre) {
        this.Nombre = Nombre;
    }

    public NbaPlayer(int Id, int Numero, String Nombre, String Equipo) {
        this.Id = Id;
        this.Numero = Numero;
        this.Nombre = Nombre;
        this.Equipo = Equipo;
    }
    
    
    //Getter & Setter

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public int getNumero() {
        return Numero;
    }

    public void setNumero(int Numero) {
        this.Numero = Numero;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getEquipo() {
        return Equipo;
    }

    public void setEquipo(String Equipo) {
        this.Equipo = Equipo;
    }
    
    
}
